package com.example.nostramovie.demo.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.nostramovie.demo.domain.Komentar;
import com.example.nostramovie.demo.exception.BadRequestAlertException;
import com.example.nostramovie.demo.service.dto.KomentarDTO;
import com.example.nostramovie.demo.services.KomentarService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class KomentarResource {
	
	public static final String ENTITY_NAME = "komentar";
	
	private final KomentarService komentarservice;
	
	public KomentarResource(KomentarService komentarservice) {
		this.komentarservice = komentarservice;
	}
	
	@ApiOperation(value = "Get All Komentar")
	@GetMapping("/komentar")
	List<Komentar>getAllKomentar(){
		return komentarservice.findAllKomentar();
	}
	
	@ApiOperation(value = "Insert New Komentar")
	@PostMapping("/komentar")
	public ResponseEntity<Boolean>createKomentar(@RequestBody KomentarDTO komentarDTO)
	throws URISyntaxException{
		log.debug("REST request to save Komentar");
		if(komentarDTO.getId()!=null) {
			 throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Boolean result = komentarservice.createKomentar(komentarDTO);
		return ResponseEntity.created(new URI("/api/komentar" + komentarDTO.getId()))
				.body(result);
	}
	@ApiOperation(value = "Update New Komentar")
	@PutMapping("/komentar")
    public ResponseEntity<Boolean> updateKomentar(@Valid @RequestBody KomentarDTO komentarDTO) throws URISyntaxException {
        log.debug("REST request to update City : {}", komentarDTO);
        if (komentarDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid ID to Update", ENTITY_NAME, "idnotexists");
        }
        Boolean result = komentarservice.updateKomentar(komentarDTO);
        return ResponseEntity.created(new URI("/api/komentar/" + komentarDTO.getId()))
               
                .body(result);
    }
	@ApiOperation(value = "Delete Komentar")
	@DeleteMapping("/komentar/{id}")
	public ResponseEntity<Boolean>deleteKomentar(@PathVariable Long id){
		log.debug("REST request to delete Komentar :{}",id);
		Boolean result = komentarservice.deleteKomentar(id);
		return ResponseEntity.ok()
				.body(result);
		
	}
}
