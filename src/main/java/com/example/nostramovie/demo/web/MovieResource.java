package com.example.nostramovie.demo.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.nostramovie.demo.domain.Movies;
import com.example.nostramovie.demo.exception.BadRequestAlertException;
import com.example.nostramovie.demo.service.dto.MoviesDTO;
import com.example.nostramovie.demo.services.FileStorageService;
import com.example.nostramovie.demo.services.MoviesService;
import com.example.nostramovie.demo.util.HeaderUtil;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("api/v1")
public class MovieResource {
	
	
	public static final String ENTITY_NAME = "movies";
	
	private final MoviesService moviesService;
	
	@Autowired
	private  FileStorageService fileStorageService;
	
	public MovieResource(MoviesService moviesService) {
		this.moviesService = moviesService; 
	}
	@ApiOperation(value = "Get All Movies")
	@GetMapping("/movies")
	public @ResponseBody List<MoviesDTO> getAllMovies(){
		
		return moviesService.findAllMovies();
	}
	
	@ApiOperation(value = "Create New Movies")
	@PostMapping("/movies")
	public ResponseEntity<Boolean>createMovies(@Valid @RequestBody MoviesDTO moviesDTO
			) throws URISyntaxException{
		
		log.debug("REST request to save Movies: {}",moviesDTO);
		if(moviesDTO.getId()!= null) {
			throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexist"); 
		}
		
		Boolean result = moviesService.createMovies(moviesDTO);
		return ResponseEntity.created(new URI("/api/movies/" + moviesDTO.getId()))
				.body(result);
		
	}
	@ApiOperation(value = "Update New Movies")
	@PutMapping("/movies")
	public ResponseEntity<Boolean>updateMovies(@RequestBody MoviesDTO moviesDTO)throws URISyntaxException{
		log.debug("REST request to update Movies:{}",moviesDTO);
		if(moviesDTO.getId()== null) {
			throw new BadRequestAlertException("Invalid ID to Update", ENTITY_NAME, "idnotexist"); 
		}
		Boolean result = moviesService.updateMovies(moviesDTO);
		return ResponseEntity.created(new URI("/api/movies/" + moviesDTO.getId()))
				.body(result);
	}
	@ApiOperation(value = "Delete  Movies")
	@DeleteMapping("/movies/{id}")
	public ResponseEntity<Boolean>deleteMovies(@PathVariable Long id){
		log.debug("REST request to delete Komentar :{}",id);
		Boolean result = moviesService.deleteMovies(id);
		return ResponseEntity.ok()
				.body(result);
		
	}
}
