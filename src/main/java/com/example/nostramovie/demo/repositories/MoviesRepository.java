package com.example.nostramovie.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.nostramovie.demo.domain.Movies;

public interface MoviesRepository extends JpaRepository<Movies, Long>{

}
