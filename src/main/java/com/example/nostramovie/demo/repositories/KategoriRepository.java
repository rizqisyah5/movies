package com.example.nostramovie.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.nostramovie.demo.domain.Kategori;
public interface KategoriRepository extends JpaRepository<Kategori, Long>{

}
