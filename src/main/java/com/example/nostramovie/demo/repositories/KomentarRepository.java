package com.example.nostramovie.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.nostramovie.demo.domain.Komentar;

public interface KomentarRepository extends JpaRepository<Komentar, Long>{

}
