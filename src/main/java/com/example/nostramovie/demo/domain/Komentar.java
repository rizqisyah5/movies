package com.example.nostramovie.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;




@Entity
@DynamicUpdate(value = true)
@Table(name = "komentar")
public class Komentar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="id_movies",nullable = false)
	private Movies movies;

	@Column(name="nama_komentar")
	private String nama_komentar;

	@Column(name="isi_komentar")
	private String isi_komentar;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama_komentar() {
		return nama_komentar;
	}

	public void setNama_komentar(String nama_komentar) {
		this.nama_komentar = nama_komentar;
	}

	public String getIsi_komentar() {
		return isi_komentar;
	}

	public void setIsi_komentar(String isi_komentar) {
		this.isi_komentar = isi_komentar;
	}

//	public Movies getMovies() {
//		movies.setComments(null);
//		return movies;
//	}
//
	public void setMovies(Movies movies) {
		this.movies = movies;
	}


}
