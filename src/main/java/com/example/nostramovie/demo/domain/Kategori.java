package com.example.nostramovie.demo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


@Entity
@Table(name ="kategori")
@DynamicUpdate(value = true)
public class Kategori implements Serializable{
	
	private static final long serialVersionUID = 7513285806618020128L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="id_movie")
	private Integer id_movie;

	@Column(name="nama_kategori")
	private String nama_kategori;
	
	public Integer getId_movie() {
		return id_movie;
	}

	public void setId_movie(Integer id_movie) {
		this.id_movie = id_movie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama_kategori() {
		return nama_kategori;
	}

	public void setNama_kategori(String nama_kategori) {
		this.nama_kategori = nama_kategori;
	}

	
}
