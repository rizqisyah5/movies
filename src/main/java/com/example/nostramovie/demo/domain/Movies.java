package com.example.nostramovie.demo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;



/**
 * A user model.
 */
@Entity
@DynamicUpdate(value = true)
@Table(name = "movies")
public class Movies{
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(fetch = FetchType.LAZY, 
			cascade = CascadeType.ALL, mappedBy = "movies")
	private List<Komentar> comments;
	
	
	@Column(name="name")
	private String name;
	

	public List<Komentar> getComments() {
		return comments;
	}


	public void setComments(List<Komentar> comments) {
		this.comments = comments;
	}


	@Column(name="description")
	private String descripton;
	
	@Column(name="rating")
	private Integer rating;
	
	@Column(name="kategori_id")
	private Integer kategori_id;
	
	@Column(name="film_full")
	private String film;
	
	@Column(name="film_trailer")
	private String film_trailer;
	
	@Column(name="film_img")
	private String film_img;
	

	@Column(name="film_img2")
	private String film_img2;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescripton() {
		return descripton;
	}


	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}


	public Integer getRating() {
		return rating;
	}


	public void setRating(Integer rating) {
		this.rating = rating;
	}


	public Integer getKategori_id() {
		return kategori_id;
	}


	public void setKategori_id(Integer kategori_id) {
		this.kategori_id = kategori_id;
	}


	public String getFilm() {
		return film;
	}


	public void setFilm(String film) {
		this.film = film;
	}


	public String getFilm_trailer() {
		return film_trailer;
	}


	public void setFilm_trailer(String film_trailer) {
		this.film_trailer = film_trailer;
	}


	public String getFilm_img() {
		return film_img;
	}


	public void setFilm_img(String film_img) {
		this.film_img = film_img;
	}


	public String getFilm_img2() {
		return film_img2;
	}


	public void setFilm_img2(String film_img2) {
		this.film_img2 = film_img2;
	}


	@Override
	public String toString() {
		return "Movies [id=" + id + ", comments=" + comments + ", name=" + name + ", descripton=" + descripton
				+ ", rating=" + rating + ", kategori_id=" + kategori_id + ", film=" + film + ", film_trailer="
				+ film_trailer + ", film_img=" + film_img + ", film_img2=" + film_img2 + "]";
	}


}
