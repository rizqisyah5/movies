package com.example.nostramovie.demo.services;

import java.util.List;

import com.example.nostramovie.demo.domain.Komentar;
import com.example.nostramovie.demo.service.dto.KomentarDTO;

public interface KomentarService {
	
	Boolean createKomentar(KomentarDTO komentarDTO);
	
	Boolean updateKomentar(KomentarDTO komentarDTO);
	
	Boolean deleteKomentar(Long id);
	
	List<Komentar>findAllKomentar();
	
}
