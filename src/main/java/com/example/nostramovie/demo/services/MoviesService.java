package com.example.nostramovie.demo.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.example.nostramovie.demo.domain.Movies;
import com.example.nostramovie.demo.service.dto.MoviesDTO;


public interface MoviesService {

	Boolean createMovies(MoviesDTO moviesDTO);
	
	Boolean updateMovies(MoviesDTO moviesDTO);
	
	Boolean deleteMovies(Long id);
	
	List<MoviesDTO> findAllMovies();
}
