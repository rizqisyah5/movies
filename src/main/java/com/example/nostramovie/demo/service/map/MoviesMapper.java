package com.example.nostramovie.demo.service.map;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.example.nostramovie.demo.domain.Movies;
import com.example.nostramovie.demo.service.dto.MoviesDTO;



@Mapper(componentModel = "spring")
public interface MoviesMapper extends EntityMapper<MoviesDTO, Movies>{
	MoviesMapper INSTANCE= Mappers.getMapper(MoviesMapper.class);

    @Override
    Movies toEntity(MoviesDTO moviesDTO);

    @Override
    MoviesDTO toDto(Movies movies);
}
