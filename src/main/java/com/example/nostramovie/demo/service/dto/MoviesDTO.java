package com.example.nostramovie.demo.service.dto;
import com.example.nostramovie.demo.domain.Komentar;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class MoviesDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7559310575060879238L;
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty(value ="description")
	private String description;
	
	@JsonProperty(value ="film_full")
	private String film;
	
	@JsonProperty(value ="kategori_id")
	private Integer kategoriId;
	
	@JsonProperty(value ="name")
	private String name;
	
	@JsonProperty(value ="rating")
	private Integer rating;
	
	@JsonProperty(value ="film_trailer")
	private String film_trailer;
	
	@JsonProperty(value ="film_img")
	private String film_img;
	
	@JsonProperty(value ="film_img2")
	private String film_img2;
//	public MoviesDTO() {
//		
//	}
	@JsonProperty("comments")
	private List<Komentar> comments;
	
	//	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFilm() {
		return film;
	}
	public void setFilm(String film) {
		this.film = film;
	}
	public Integer getKategoriId() {
		return kategoriId;
	}
	public void setKategoriId(Integer kategoriId) {
		this.kategoriId = kategoriId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getFilm_trailer() {
		return film_trailer;
	}
	public void setFilm_trailer(String film_trailer) {
		this.film_trailer = film_trailer;
	}
	public String getFilm_img() {
		return film_img;
	}
	public void setFilm_img(String film_img) {
		this.film_img = film_img;
	}
	public String getFilm_img2() {
		return film_img2;
	}
	public void setFilm_img2(String film_img2) {
		this.film_img2 = film_img2;
	}
	public List<Komentar> getComments() {
		return comments;
	}
	public void setComments(List<Komentar> list) {
		this.comments = list;
	}
//	@Override
//	public String toString() {
//		return "MoviesDTO {id=" + id + ", description=" + description + ", film=" + film + ", kategoriId=" + kategoriId
//				+ ", name=" + name + ", rating=" + rating + ", film_trailer=" + film_trailer + ", film_img=" + film_img
//				+ ", film_img2=" + film_img2 + "}";
//	}
	

}
