package com.example.nostramovie.demo.service.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KomentarDTO implements Serializable{
	
	private static final long serialVersionUID = 7559310575060879238L;
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty(value ="id_movies")
	private Long id_movies;
	
	@JsonProperty(value ="nama_komentar")
	private String nama_komentar;
	
	@JsonProperty(value="isi_komentar")
	private String isi_komentar;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama_komentar() {
		return nama_komentar;
	}

	public void setNama_komentar(String nama_komentar) {
		this.nama_komentar = nama_komentar;
	}

	public String getIsi_komentar() {
		return isi_komentar;
	}

	public void setIsi_komentar(String isi_komentar) {
		this.isi_komentar = isi_komentar;
	}

	public Long getId_movies() {
		return id_movies;
	}

	public void setId_movies(Long id_movies) {
		this.id_movies = id_movies;
	}

	public String toString() {
		return "KomentarDTO {id=" + id + ", id_movies=" + id_movies + ", nama_komentar=" + nama_komentar
				+ ", isi_komentar=" + isi_komentar + "}";
	}
	
}
