package com.example.nostramovie.demo.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.nostramovie.demo.domain.Komentar;
import com.example.nostramovie.demo.domain.Movies;
import com.example.nostramovie.demo.exception.BadRequestAlertException;
import com.example.nostramovie.demo.repositories.KomentarRepository;
import com.example.nostramovie.demo.repositories.MoviesRepository;
import com.example.nostramovie.demo.service.dto.KomentarDTO;
import com.example.nostramovie.demo.services.KomentarService;
import com.example.nostramovie.demo.util.ErrorConstants;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
@Transactional
public class KomentarServiceImp implements KomentarService{
	
	@Autowired
	private  KomentarRepository komentarRepository;
	
	@Autowired
	private MoviesRepository moviesRepository;
	 ModelMapper modelMapper = new ModelMapper();

	
	@Override
	public Boolean createKomentar(KomentarDTO komentarDTO) {
		// TODO Auto-generated method stub
		Movies movies = moviesRepository.findById(komentarDTO.getId_movies())
				 .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Movies Id Not found!", "Movies", "MoviesExist"));
		Komentar komentar = new Komentar();
		komentar.setMovies(movies);
		komentar.setIsi_komentar(komentarDTO.getNama_komentar());
		komentar.setNama_komentar(komentarDTO.getIsi_komentar());
		komentarRepository.save(komentar);
		return Boolean.TRUE;
	}

	@Override
	public Boolean updateKomentar(KomentarDTO komentarDTO) {
		// TODO Auto-generated method stub
		Komentar komentar = komentarRepository.findById(komentarDTO.getId())
				.orElseThrow(()-> new BadRequestAlertException
						(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Komentar Id Not Found", "Komentar","KomentarExist"));
//		modelMapper.map(cityDTO,city);
		Movies movies = moviesRepository.findById(komentarDTO.getId_movies())
				 .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Movies Id Not found!", "Movies", "MoviesExist"));
		komentar.setMovies(movies);
		komentar.setIsi_komentar(komentarDTO.getNama_komentar());
		komentar.setNama_komentar(komentarDTO.getIsi_komentar());
		return Boolean.TRUE;
	}

	@Override
	public Boolean deleteKomentar(Long id) {
		// TODO Auto-generated method stub
		komentarRepository.deleteById(id);
		return Boolean.TRUE;
	}

	@Override
	public List<Komentar> findAllKomentar() {
		// TODO Auto-generated method stub
		
		return komentarRepository.findAll();
	}

	
	

}
