package com.example.nostramovie.demo.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
//import java.util.Base64;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.example.nostramovie.demo.domain.Movies;
import com.example.nostramovie.demo.exception.BadRequestAlertException;
import com.example.nostramovie.demo.repositories.MoviesRepository;
import com.example.nostramovie.demo.service.dto.MoviesDTO;
import com.example.nostramovie.demo.service.map.MoviesMapper;
import com.example.nostramovie.demo.services.FileStorageService;
import com.example.nostramovie.demo.services.MoviesService;
import com.example.nostramovie.demo.util.Base64Util;
import com.example.nostramovie.demo.util.ErrorConstants;
import com.example.nostramovie.demo.util.FileStorage;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
public class MoviesServicesImp implements MoviesService {
	
	private final MoviesRepository moviesRepository;
	
	@Value("${file.upload-dir}")
	private  String path;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	public MoviesServicesImp(MoviesRepository moviesRepository) {
		this.moviesRepository = moviesRepository;
	}


	@Override
	public List<MoviesDTO> findAllMovies() {
		// TODO Auto-generated method stub
		
		List<Movies> movieList =moviesRepository.findAll();
		List<MoviesDTO> mDTOs= new ArrayList();
		for(Movies m : movieList) {
			MoviesDTO mDTO = new MoviesDTO();
			mDTO.setId(m.getId());
			mDTO.setDescription(m.getDescripton());
			mDTO.setFilm(m.getFilm());
			mDTO.setFilm_img(m.getFilm_img());
			mDTO.setFilm_img2(m.getFilm_img2());
			mDTO.setKategoriId(m.getKategori_id());
			mDTO.setName(m.getName());
			mDTO.setRating(m.getRating());
			mDTO.setComments(m.getComments());
			mDTOs.add(mDTO);
		}
		return mDTOs;
	}
	@Override
	public Boolean createMovies(MoviesDTO moviesDTO) {
		// TODO Auto-generated method stub	
		String film_img = moviesDTO.getFilm_img();
//		String film_img2 =  moviesDTO.getFilm_img2();
		
		byte[] film_imgByte = DatatypeConverter.parseBase64Binary(film_img);
//		byte[] film_imgByte2 = DatatypeConverter.parseBase64Binary(film_img2);
		
		Movies movies = new Movies();
		//Penggunaan Map
//		Movies movies = MoviesMapper.INSTANCE.toEntity(moviesDTO);
		
		movies.setDescripton(moviesDTO.getDescription());
//		movies.setFilm(Base64Util.writeByte(base64decodedBytes,path));
		movies.setFilm_img(Base64Util.writeByte(film_imgByte,path));
//		movies.setFilm_img2(moviesDTO.getId()+"_"+Base64Util.writeByte(film_imgByte2,path));
//		movies.setFilm_trailer(fileStorageService.storeFile(file));
//		movies.setFilm(filename);
		
		movies.setKategori_id(moviesDTO.getKategoriId());
		movies.setName(moviesDTO.getName());
		movies.setRating(moviesDTO.getRating());
		moviesRepository.save(movies);
		return Boolean.TRUE;
	}
	@Override
	@Transactional
	public Boolean updateMovies(MoviesDTO moviesDTO) {
		// TODO Auto-generated method stub
		Movies movies  = moviesRepository.findById(moviesDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Movies Id Not found!", "Movies", "MoviesExist"));
		movies.setDescripton(moviesDTO.getDescription());
		movies.setFilm(moviesDTO.getFilm());
		movies.setKategori_id(moviesDTO.getKategoriId());
		movies.setName(moviesDTO.getName());
		movies.setRating(moviesDTO.getRating());
		moviesRepository.save(movies);
		return Boolean.TRUE;
	}

	@Override
    public Boolean deleteMovies(Long id) {
		moviesRepository.deleteById(id);
        return Boolean.TRUE;
    }

}
